FROM --platform=$TARGETPLATFORM docker.io/library/node:16-alpine as deemix

ARG TARGETPLATFORM
ARG BUILDPLATFORM

RUN echo "Building for TARGETPLATFORM=$TARGETPLATFORM | BUILDPLATFORM=$BUILDPLATFORM"
RUN apk add --no-cache git jq python3 make gcc musl-dev g++ && \
    rm -rf /var/lib/apt/lists/*
COPY . /app
WORKDIR app
RUN case "$TARGETPLATFORM" in \
        "linux/amd64") \
            jq '.pkg.targets = ["node16-alpine-x64"]' ./server/package.json > tmp-json ;; \
        "linux/arm64") \
            jq '.pkg.targets = ["node16-alpine-arm64"]' ./server/package.json > tmp-json ;; \
        "linux/arm64/v8") \
            jq '.pkg.targets = ["node16-alpine-arm64"]' ./server/package.json > tmp-json ;; \
        *) \
            echo "Platform $TARGETPLATFORM not supported" && exit 1 ;; \
    esac && \
    mv tmp-json /app/server/package.json
RUN yarn install-all
RUN yarn dist-server
RUN mv /app/dist/deemix-server /deemix-server

FROM lsiobase/alpine:3.19

COPY --from=deemix /deemix-server /deemix-server
COPY docker-root/ /

EXPOSE 6595
ENTRYPOINT [ "/init" ]
