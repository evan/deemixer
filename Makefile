docker_build_local:
	docker build -t deemixer:latest .

docker_build_release_latest:
	docker buildx build \
		--platform linux/amd64,linux/arm64 \
		-t gitea.va.reichard.io/evan/deemixer:latest \
		--push .
