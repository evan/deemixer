# Deemixer

This is forked from [deemix-webui](https://gitlab.com/RemixDev/deemix-webui) and [deemix-gui](https://gitlab.com/RemixDev/deemix-webui).

The submodule reference was removed and consolidated into this single repo. Git history was maintained.

## Building Docker Image

    make docker_build_local

## Running Docker Image

    docker run -d \
      -v ./downloads/:/downloads \
      -v ./config:/config \
      -e PUID=1000 \
      -e PGID=1000 \
      -e UMASK_SET=022 \
      -e DEEMIX_SINGLE_USER=true \
      -p 6595:6595 \
      gitea.va.reichard.io/evan/deemixer:latest

## Running from source

You need to use nodejs 16.x, using `yarn` is recommended.

Install the dependencies using `yarn install-all` for production.
Install the dependencies using `yarn install-all-dev` for development.
Then you should be able to run the app with `yarn start`.
If you want to further develop deemix-gui and propose a PR, use `yarn dev`

Commands for easy setup:

```sh
# Production
git clone https://gitea.va.reichard.io/evan/Deemixer.git . && yarn install-all
# Development
git clone https://gitea.va.reichard.io/evan/Deemixer.git . && yarn install-all-dev
```

You can change the default port by setting the environment variable `PORT` to any other number before starting the app.

## Building the app

To build the app you need to have git installed and the repo cloned with `git`.
Make sure you've installed the dependencies for all packages (the root folder, `server` and `webui`).
Then from the root folder run `yarn dist` to make a distributable package for your current OS or `yarn dist-server` to make an executable for only the server.

## Feature requests

Before asking for a feature make sure it isn't an already open issue on the repo

# License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
